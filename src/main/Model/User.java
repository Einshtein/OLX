package main.Model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="user")
public class User {
  @Id
  @GeneratedValue
  private long id;
  @Column(name = "login")
  @Size(min = 4, max = 20, message = "Логин должен быть от 4 до 20 символов!")
  private String login;
  @Column(name = "password")
  @Size(min = 4, max = 32, message = "Пароль должен быть от 4 до 32 символов!")
  @NotNull
  private String password;
  @Column(name = "name")
  @NotNull
  private String name;
  @Column(name = "surname")
  @NotNull
  private String surname;


  public User(String login, String name, String password, String surname) {
    this.login = login;
    this.name = name;
    this.password = password;
    this.surname = surname;
  }

  public User() {
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getLogin() {
    return this.login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPassword() {
    return this.password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getSurname() {
    return this.surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }
}

  
