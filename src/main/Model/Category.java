package main.Model;

import javax.persistence.*;

/**
 * Created by Евгений on 08.08.2016.
 */
@Entity
@Table(name="category")
public class Category {
    @Id
    @GeneratedValue
    private long id;
    @Column(name = "name")
    private String name;

    public Category(String name) {
        this.name = name;
    }

    public Category() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(name);
        return sb.toString();
    }
}
