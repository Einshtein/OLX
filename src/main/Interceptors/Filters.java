package main.Interceptors;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import main.Model.User;

@WebFilter(filterName="Filter", urlPatterns={"/spring/register"})
public class Filters
  implements Filter
{
  public void destroy() {}
  
  public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
    throws ServletException, IOException
  {
    HttpServletRequest request = (HttpServletRequest)req;
    HttpServletResponse response = (HttpServletResponse)resp;
    User user = (User)request.getSession().getAttribute("user");
    if ((user != null) && (user.getName().equals("sdfsdf")))
    {
      chain.doFilter(req, resp);
    }
    else
    {
      request.getSession().setAttribute("url", request.getRequestURI());
      request.getSession().setAttribute("message", "You have to be admin");
      response.sendRedirect(request.getContextPath() + "/login.jsp");
    }
  }
  
  public void init(FilterConfig config)
    throws ServletException
  {}
}
