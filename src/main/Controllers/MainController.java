package main.Controllers;

import main.Dao.AdvDAO;
import main.Model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping({"/"})
public class MainController
{
  //настройка вида отображения объявления - списком(2) или плитками(1)
  public static final Integer BARVIEW = 1;
  public static final Integer LISTVIEW = 2;
  @Autowired
  private AdvDAO advDAO;

  @RequestMapping(value={"/"}, method={RequestMethod.GET})
  public ModelAndView listAdvs(Model model)
  {
    //по умолчанию объявления отображается списком
    model.addAttribute("view", LISTVIEW);
    return new ModelAndView("index", "advs", advDAO.getAdv());
  }

  @RequestMapping(value={"/view"}, method={RequestMethod.GET})
  public ModelAndView viewType(@RequestParam("view") Integer view, Model model)
  {
    model.addAttribute("view", view);
    return new ModelAndView("index", "advs", advDAO.getAdv());
  }
  
  @RequestMapping(value={"/login"}, method={RequestMethod.GET})
  public ModelAndView login()
  {
    return new ModelAndView("login", "user", new User());
  }

  @RequestMapping(value={"/logout"}, method={RequestMethod.GET})
  public String logout()
  {
    return "logout";
  }
  
  @RequestMapping({"/image/{file_id}"})
  public void getFile(HttpServletRequest request, HttpServletResponse response, @PathVariable("file_id") long fileId)
  {
    try
    {
      byte[] content = advDAO.getPhoto(fileId);
      response.setContentType("image/jpg");
      response.getOutputStream().write(content);
      response.getOutputStream().flush();
      response.getOutputStream().close();
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }
  
  @RequestMapping(value={"/search"}, method={RequestMethod.GET})
  public ModelAndView search(@RequestParam("pattern") String pattern, Model model, HttpServletRequest request)
  {
    model.addAttribute("view", LISTVIEW);
    if (advDAO.getAdv(pattern).size() == 0) {
      request.getSession().setAttribute("message", "Не найдено");
    }
    return new ModelAndView("index", "advs", advDAO.getAdv(pattern));
  }

  @RequestMapping(value={"/show"}, method={RequestMethod.GET})
  public ModelAndView showAdvertisement(@RequestParam("id") long id)
  {
    return new ModelAndView("show", "advs", advDAO.getAdvById(id));
  }
}
