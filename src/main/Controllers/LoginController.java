package main.Controllers;

import main.Dao.AdvDAO;
import main.Dao.UserDAO;
import main.Model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

@Controller
@RequestMapping({"/"})
public class LoginController {
  @Autowired
  private AdvDAO advDAO;
  @Autowired
  private UserDAO userDAO;
  @Autowired
  private RegisterController registerController;

  @RequestMapping(value = {"/logged"}, method = {RequestMethod.POST})
  public String login(@Valid @ModelAttribute("user") User user, BindingResult bindingResult, Model model, HttpServletRequest request, HttpServletResponse response) {
    if (bindingResult.hasErrors()) {
      model.addAttribute("message", "Неверный логин или пароль!");
      return "login";
    }
    User newUser = null;
    try
    {
    model.addAttribute("user", user);
    newUser = userDAO.getUserByLogin(user.getLogin());
    }
    catch (IndexOutOfBoundsException e)
    {
      e.printStackTrace();
    }
    try {
      if ((newUser != null) && (registerController.encryptPassword(user.getPassword()).equals(newUser.getPassword()))) {
        request.getSession().setAttribute("user", newUser);
//        response.sendRedirect(request.getContextPath() + "/MyProfile/");
        return "redirect:/MyProfile/";
      }
        if ((newUser == null) || (!registerController.encryptPassword(user.getPassword()).equals(newUser.getPassword()))) {
//          model.addAttribute("message", "Неверный логин или пароль!");
          request.getSession().setAttribute("message", "Неверный логин или пароль!");
//          response.sendRedirect(request.getContextPath() + "/login");
          return "redirect:/login";


        }
    }
    catch (IOException e) {
      e.printStackTrace();
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    return null;
  }
}