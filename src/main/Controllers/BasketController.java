package main.Controllers;

import main.Dao.AdvDAO;
import main.Dao.AdvDAOImpl;
import main.Model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping({"/MyProfile"})
public class BasketController
{
  @Autowired
  private AdvDAO advDAO;
  
  @RequestMapping({"/truncate"})
  public ModelAndView truncate(@RequestParam("id") long id, HttpServletRequest request)
  {
    User user = (User)request.getSession().getAttribute("user");
    advDAO.truncateAdv(id);
    if (advDAO.getAdvFromBasketByUserId(user.getId()).size() == 0) {
      request.getSession().setAttribute("message", "Не найдено");
    }
    if(AdvDAOImpl.success) {
      return new ModelAndView("basket", "advs", advDAO.getAdvFromBasketByUserId(user.getId()));
    } else {
      return new ModelAndView("error", "message", "При удалении объявления произошла ошибка!");
    }
  }
  
  @RequestMapping({"/basket"})
  public ModelAndView listBasket(HttpServletRequest request, HttpServletResponse response)
  {
    User user = (User) request.getSession().getAttribute("user");
      if (user == null) {
        MyProfileController.isAuthorised(request, response);
        return null;
      }
    if (user != null) {
      request.getSession().setAttribute("message", "");
      if (advDAO.getAdvFromBasketByUserId(user.getId()).size() == 0) {
        request.getSession().setAttribute("message", "Не найдено");
      }
    }
    return new ModelAndView("basket", "advs", advDAO.getAdvFromBasketByUserId(user.getId()));
  }
  
  @RequestMapping({"/restore"})
  public ModelAndView restore(@RequestParam("id") long id, HttpServletRequest request)
  {
    User user = (User)request.getSession().getAttribute("user");
    advDAO.restoreAdv(id, request);
    if(AdvDAOImpl.success) {
      return new ModelAndView("basket", "advs", advDAO.getAdvFromBasketByUserId(user.getId()));
    } else {
      return new ModelAndView("error", "message", "При восстановлении объявления произошла ошибка!");
    }

  }
  
  @RequestMapping({"/restoreSelected"})
  public ModelAndView restoreByCheckbox(@RequestParam("idList") List<Long> idList, HttpServletRequest request)
  {
    User user = (User)request.getSession().getAttribute("user");
    advDAO.restoreAdvByCheckbox(idList, request);
    if(AdvDAOImpl.success) {
      return new ModelAndView("basket", "advs", advDAO.getAdvFromBasketByUserId(user.getId()));
    } else {
      return new ModelAndView("error", "message", "При восстановлении объявления произошла ошибка!");
    }
  }
}
