package main.Controllers;

import main.Dao.UserDAO;
import main.Model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Controller
@RequestMapping({"/"})
public class RegisterController
{
  public static final int WEAK = 1;
  public static final int MIDDLE = 5;
  public static final int STRONG = 7;

  public static final String WEAK_COLOR = "red";
  public static final String MIDDLE_COLOR = "yellow";
  public static final String STRONG_COLOR = "green";

  @Autowired
  private UserDAO userDAO;
  
  @RequestMapping(value={"/register"}, method={RequestMethod.GET})
  public ModelAndView register()
  {
    return new ModelAndView("register", "user", new User());
  }

  @RequestMapping(value={"/registered"}, method={RequestMethod.POST})
  public String addUser(@Valid @ModelAttribute("user") User user, BindingResult bindingResult, Model model,
                              HttpServletRequest request, HttpServletResponse response) {
    if (bindingResult.hasErrors()) {
      return "register";
    }
    User newUser = null;
    try {
      model.addAttribute("message", "Вы успешно зарегистрировались!");
      newUser = new User(user.getLogin(), user.getName(), encryptPassword(user.getPassword()), user.getSurname());
      userDAO.add(newUser);
      return "registered";
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    userDAO.add(newUser);


    return null;
  }

//
//  @RequestMapping(value={"/registered"}, method={RequestMethod.POST})
//  public ModelAndView addUser(@RequestParam("password") String password,
//                              @RequestParam("surname") String surname,
//                              @RequestParam("login") String login,
//                              @RequestParam("password") String password,
//                              HttpServletRequest request,
//                              HttpServletResponse response)
//  {
//    User user = null;
//    try {
//      user = new User(login, password, encryptPassword(password), surname);
//      userDAO.add(user);
//      return new ModelAndView("registered", "message", "Вы успешно зарегистрированы!");
//    } catch (NoSuchAlgorithmException e) {
//      e.printStackTrace();
//    } catch (UnsupportedEncodingException e) {
//      e.printStackTrace();
//    }
//    return null;
//  }

@RequestMapping(value = "/register/checkPassword", method = RequestMethod.GET, produces = {"text/html; charset=UTF-8"})
public @ResponseBody String checkPassword (@RequestParam String password) {
  String result = "<span style=\"color:%s; font-weight:bold;\">%s<span>";
  if (password.length() >= WEAK & password.length() < MIDDLE) {
    return String.format(result,WEAK_COLOR,"Пароль слабый");
  } else if (password.length() >= MIDDLE & password.length() < STRONG) {
    return String.format(result,MIDDLE_COLOR,"Пароль средний");
  } else if (password.length() >= STRONG) {
    return String.format(result,STRONG_COLOR,"Пароль сильный");
  }
  return "";
}

  public String encryptPassword(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {

    final StringBuilder sbMd5Hash = new StringBuilder();
    final MessageDigest m = MessageDigest.getInstance("MD5");
    m.update(password.getBytes("UTF-8"));

    final byte data[] = m.digest();

    for (byte element : data) {
      sbMd5Hash.append(Character.forDigit((element >> 4) & 0xf, 16));
      sbMd5Hash.append(Character.forDigit(element & 0xf, 16));
    }

    return sbMd5Hash.toString();
  }
}
