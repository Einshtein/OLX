package main.Controllers;

import main.Dao.AdvDAO;
import main.Model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping({"/MyProfile"})
public class SearchController
{
  @Autowired
  private AdvDAO advDAO;
  //настройка вида отображения объявления - списком(2) или плитками(1)
  public static final Integer BARVIEW = 1;
  public static final Integer LISTVIEW = 2;
  
  @RequestMapping(value={"/search"}, method={RequestMethod.GET})
  public ModelAndView search(@RequestParam("pattern") String pattern, HttpServletRequest request, Model model)
  {
    model.addAttribute("view", LISTVIEW);
    if (advDAO.getUserAdvByPattern(pattern).size() == 0) {
      request.getSession().setAttribute("message", "Не найдено");
    }
    return new ModelAndView("myprofile", "advs", advDAO.getUserAdvByPattern(pattern));
  }
  
  @RequestMapping(value={"/searchBasket"}, method={RequestMethod.GET})
  public ModelAndView searchBasket(@RequestParam("pattern") String pattern, HttpServletRequest request, Model model)
  {
    model.addAttribute("view", LISTVIEW);
    User user = (User)request.getSession().getAttribute("user");
    if (advDAO.getAllBasketAdv(pattern, user.getId()).size() == 0) {
      request.getSession().setAttribute("message", "Не найдено");
    }
    return new ModelAndView("basket", "advs", advDAO.getAllBasketAdv(pattern, user.getId()));
  }
}
