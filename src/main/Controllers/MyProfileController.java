package main.Controllers;

import main.Dao.AdvDAO;
import main.Dao.AdvDAOImpl;
import main.Dao.UserDAO;
import main.Model.Advertisement;
import main.Model.Category;
import main.Model.Photo;
import main.Model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping({"/MyProfile"})
public class MyProfileController
{
  @Autowired
  private EntityManager entityManager;
  @Autowired
  private AdvDAO advDAO;
  @Autowired
  private UserDAO userDAO;
  
  @RequestMapping({"/"})
  public ModelAndView listAdvs(HttpServletRequest request, HttpServletResponse response) {
    User user = (User) request.getSession().getAttribute("user");
    if ((user == null)) {
      isAuthorised(request, response);
      return null;
    }
    if (user != null) {
      request.getSession().setAttribute("message", "");
    }
    return new ModelAndView("myprofile", "advs", advDAO.getUserAdvByUserId(user.getId()));
  }
  
  @RequestMapping(value={"/add_page"}, method={RequestMethod.GET})
  public ModelAndView add(HttpServletRequest request, HttpServletResponse response)
  {
    User user = (User) request.getSession().getAttribute("user");
    if (user == null) {
      isAuthorised(request, response);
      return null;
    }
    return new ModelAndView("add_page", "categoryList", advDAO.getAllCategory());
  }
  
  @RequestMapping({"/delete"})
  public ModelAndView delete(@RequestParam("id") long id, HttpServletRequest request)
  {
    User user = (User)request.getSession().getAttribute("user");
    advDAO.deleteAdv(id, request);
    if(AdvDAOImpl.success) {
      return new ModelAndView("myprofile", "advs", advDAO.getUserAdvByUserId(user.getId()));
    } else {
      return new ModelAndView("error", "message", "При удалении объявления произошла ошибка!");
    }
  }
  
  @RequestMapping({"/deleteSelected"})
  public ModelAndView deleteByCheckbox(@RequestParam("idList") List<Long> idList, HttpServletRequest request)
  {
    User user = (User)request.getSession().getAttribute("user");
    advDAO.deleteAdvByCheckbox(idList,request);
    if(AdvDAOImpl.success) {
      return new ModelAndView("myprofile", "advs", advDAO.getUserAdvByUserId(user.getId()));
    } else {
      return new ModelAndView("error", "message", "При удалении объявления произошла ошибка!");
    }
  }
  
  @RequestMapping({"/image/{file_id}"})
  public void getFile(HttpServletRequest request, HttpServletResponse response, @PathVariable("file_id") long fileId)
  {
    try
    {
      byte[] content = advDAO.getPhoto(fileId);
      response.setContentType("image/jpg");
      response.getOutputStream().write(content);
      response.getOutputStream().flush();
      response.getOutputStream().close();
    }
    catch (IOException ex)
    {
      ex.printStackTrace();
    }
  }
  
  @RequestMapping(value={"/add"}, method={RequestMethod.POST})
  public ModelAndView addAdv(@RequestParam("name") String name,
                             @RequestParam("shortDesc") String shortDesc,
                             @RequestParam(value="longDesc", required=false) String longDesc,
                             @RequestParam("phone") String phone,
                             @RequestParam("price") double price,
                             @RequestParam("photo") MultipartFile photo,
                             @RequestParam("category") String categoryName,
                             HttpServletRequest request,
                             HttpServletResponse response)
  {
    try
    {
      Advertisement adv = new Advertisement(name, shortDesc, longDesc, phone, price,
              photo.isEmpty() ? null : new Photo(photo.getOriginalFilename(), photo.getBytes()));
      User user = (User)request.getSession().getAttribute("user");
      Category category = advDAO.getCategoryByName(categoryName);
//      Category category = new Category("tovar");
      adv.setUser(user);
      adv.setCategory(category);
      advDAO.addAdv(adv);
     if(AdvDAOImpl.success) {
       return new ModelAndView("myprofile", "advs", advDAO.getUserAdvByUserId(user.getId()));
     } else {
       return new ModelAndView("error", "message", "При добавлении объявления произошла ошибка (возможно, фото слишком большое!)");
     }
    }
    catch (IOException ex)
    {
      response.setStatus(500);
    }
    return null;
  }

  @RequestMapping(value={"/show_my"}, method={RequestMethod.GET})
  public ModelAndView showAdvertisement(@RequestParam("id") long id, HttpServletRequest request, HttpServletResponse response)
  {
    User user = (User) request.getSession().getAttribute("user");
      if (user == null) {
        isAuthorised(request, response);
        return null;
      }
    return new ModelAndView("show_my", "advs", advDAO.getUserAdvById(id));
  }

  @RequestMapping(value={"/updateAdv"}, method={RequestMethod.POST})
  public ModelAndView updateAdv(@RequestParam("name") String name,
                             @RequestParam("shortDesc") String shortDesc,
                             @RequestParam(value="longDesc", required=false) String longDesc,
                             @RequestParam("phone") String phone,
                             @RequestParam("price") double price,
                             @RequestParam("photo") MultipartFile photo,
                             @RequestParam("id") Integer id,
                             HttpServletRequest request,
                             HttpServletResponse response)
  {
    try
    {
//      Advertisement adv = new Advertisement(name, shortDesc, longDesc, phone, price,
//              photo.isEmpty() ? null : new Photo(photo.getOriginalFilename(), photo.getBytes()));
      User user = (User)request.getSession().getAttribute("user");
      entityManager.getTransaction().begin();
      Advertisement adv = (Advertisement)entityManager.find(Advertisement.class, Long.valueOf(id));
      adv.setName(name);
      adv.setLongDesc(longDesc);
      adv.setShortDesc(shortDesc);
      adv.setPrice(price);
      adv.setPhone(phone);
      if(!photo.isEmpty()) {
        adv.setPhoto(new Photo(photo.getOriginalFilename(), photo.getBytes()));
      }
      entityManager.getTransaction().commit();
      if(AdvDAOImpl.success) {
        return new ModelAndView("myprofile", "advs", advDAO.getUserAdvByUserId(user.getId()));
      } else {
        return new ModelAndView("error", "message", "При изменении объявления произошла ошибка (возможно, фото слишком большое!)");
      }
    }
    catch (IOException ex)
    {
      response.setStatus(500);
    }
    return null;
  }

  public static void isAuthorised(HttpServletRequest request, HttpServletResponse response){
      try {
        request.getSession().setAttribute("message", "Пожалуйста, ведите логин и пароль для входа или зарегистрируйтесь!");
        response.sendRedirect(request.getContextPath() + "/login");
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
}
