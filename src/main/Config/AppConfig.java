package main.Config;

import main.Dao.AdvDAO;
import main.Dao.AdvDAOImpl;
import main.Dao.UserDAO;
import main.Dao.UserDAOImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

@Configuration
@ComponentScan({"main"})
@EnableWebMvc
public class AppConfig
  extends WebMvcConfigurerAdapter
{
  public void addResourceHandlers(ResourceHandlerRegistry registry)
  {
    registry.addResourceHandler("/**").addResourceLocations("/");
  }
  
  @Bean
  public EntityManager entityManager()
  {
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("AdvJPA");
    return emf.createEntityManager();
  }
  
  @Bean
  public AdvDAO advDAO()
  {
    return new AdvDAOImpl();
  }
  
  @Bean
  public UserDAO userDAO()
  {
    return new UserDAOImpl();
  }
  
  @Bean
  public InternalResourceViewResolver setupViewResolver()
  {
    InternalResourceViewResolver resolver = new InternalResourceViewResolver();
    resolver.setPrefix("/WEB-INF/pages/");
    resolver.setSuffix(".jsp");
    resolver.setViewClass(JstlView.class);
    resolver.setOrder(1);
    return resolver;
  }
  
  @Bean
  public CommonsMultipartResolver multipartResolver()
  {
    return new CommonsMultipartResolver();
  }
}
