<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>Welcome page</title>
    <meta charset="UTF-8" />
    <link href="<c:url value="/css/style.css" />" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
</head>
<body>
<h1 align = "center" style="font-size: 25px; margin-top: 150px;">${message}</h1>
<div align="center">
<input type="button" class="btn btn-success login_button" value="Назад" onclick="history.back()">
</div>
<jsp:include page="HF/Footer.jsp"></jsp:include>