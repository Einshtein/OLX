<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Доска объявлений</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <link href="<c:url value="/css/my_profile_style.css" />" rel="stylesheet" type="text/css" />

</head>
<body>
<div class="header_container">
    <div class="title" style="text-align: center;"><a href="${pageContext.servletContext.contextPath}/">
        <font face="Arial" size="9pt" ><i><b>Доска объявлений</b></i></font></a>
        <c:if test="${user eq null}">
        <form class="form-inline" role="form" action="${pageContext.servletContext.contextPath}/MyProfile/" method="get">
            <input type="submit" class="btn btn-success login_button" value="Войти">
        </form>
        <form class="form-inline" role="form" action="${pageContext.servletContext.contextPath}/register/" method="get">
                <input type="submit" class="btn btn-success register_button" value="Регистрация">
        </form>
        </c:if>
    </div>
<c:if test="${user ne null}">
    <div class="menu_container" align="center">
        <ul class="menu">
            <a href="${pageContext.servletContext.contextPath}/MyProfile/">
            <li style="width:${(100/4)-1}%;">
                Мой профиль
            </li></a>
        </ul>
    </div>
</c:if>
</div>
<div class="container">

        <h3>Объявления:</h3>
    <c:if test="${fn:length(advs) eq 0}">
        <div align="center"><h2>${message}</h2></div>
    </c:if>
    <c:if test="${fn:length(advs) ne 0}">
        <form class="form-inline" role="form" action="${pageContext.servletContext.contextPath}/search" method="get">
            <input type="text" class="form-control" name="pattern" placeholder="Поиск">
            <input type="submit" class="btn btn-default" value="Поиск">
        </form>


        <div class="sort">
            <div class="view-tile-list">
    <span class="btn-link btn-link-gray g-tile-btn-link active">
        <a class="btn-link-ii" href="${pageContext.servletContext.contextPath}/view?view=1">
            <img class="sprite g-tile-btn-link-icon" width="14" height="15" title="Плиткой" src="http://i.rozetka.ua/design/_.gif" alt="Плиткой"></img>
        </a>
    </span>
			<span class="btn-link btn-link-gray g-tile-btn-link active">
			<a class="btn-link-i" href="${pageContext.servletContext.contextPath}/view?view=2">
                <img class="sprite g-list-btn-link-icon" width="14" height="15" title="Списком" src="http://i.rozetka.ua/design/_.gif" alt="Списком"></img>
            </a>
			</span>
            </div>
        </div>

        <c:if test="${view eq 1}">
            <c:forEach items="${advs}" var="adv">
            <div class="show_bar">
                <table class="show_adv" width="25%" border="0" cellspacing="0" cellpadding="0" style="float:left; padding:20px;">
                    <tr><td>
                        <div class="show_adv_img_bar"><img class="show_adv_img_bar_img" src="${pageContext.servletContext.contextPath}/image/${adv.photo.id}" width="200px" height="300px" alt="" border="0" />
                            <div class="show_adv_adv_text_bar">
                                <div class="show_adv_info_bar">
                                    <i><b>Название: </b></i><a href="${pageContext.servletContext.contextPath}/show?id=${adv.id}">${adv.name}</a>
                                </div>
                                <div class="show_adv_info_bar">
                                    <i><b>Крат. описание: </b></i>${adv.shortDesc}
                                </div>
                                <div class="show_adv_info_bar">
                                    <i><b>Цена: </b></i>${adv.price}
                                </div>
                                <div class="show_adv_info_bar">
                                    <i><b>Категория: </b></i>${adv.category.name}
                                </div>
                            </div>
                        </div>
                    </td></tr>
                </table>
            </c:forEach>
            </div>
        </c:if>
        <c:if test="${view eq 2}">
        <table class="table table-striped">
            <thead>
            <tr>
                <td><b>Фото</b></td>
                <td><b>Название</b></td>
                <td><b>Крат. описание</b></td>
                <td><b>Цена</b></td>
                <td><b>Категория</b></td>
            </tr>
            </thead>
                <c:forEach items="${advs}" var="adv">
                <tr>
                    <td> <c:if test="${adv.photo ne null}">
                        <img height="40" width="40" src="${pageContext.servletContext.contextPath}/image/${adv.photo.id}" /></td>
                    </c:if>
                    <td><a href="${pageContext.servletContext.contextPath}/show?id=${adv.id}">${adv.name}</a></td>
                    <td>${adv.shortDesc}</td>
                    <td>${adv.price}</td>
                    <td>${adv.category.name}</td>
                </tr>
                </c:forEach>
        </table>
        </c:if>
    </c:if>
</div>
<jsp:include page="HF/Footer.jsp"></jsp:include>