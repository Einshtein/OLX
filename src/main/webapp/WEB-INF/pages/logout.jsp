<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <link href="<c:url value="/css/my_profile_style.css" />" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="header_container">
    <div class="title" style="text-align: center;"><a href="${pageContext.servletContext.contextPath}/">
        <font face="Arial" size="9pt" ><i><b>Доска объявлений</b></i></font></a></div>
    <div class="menu_container" align="center">
        <ul class="menu">
            <li style="width:${(100/4)-1}%;">
                <a href="${pageContext.servletContext.contextPath}/">На главную</a></li>
        </ul>
    </div>
</div>
<%
    HttpSession newsession = request.getSession(false);
    if (newsession != null && request.getSession().getAttribute("user")!=null)
    {
        newsession.invalidate();
        out.println("<div align = 'center'><h1><font color=\"Red\">Вы успешно разлогинились</font></h1></div>");
%>
<%    }
else {
    out.println("<div align = 'center'><h1><font color=\"Red\">Вы не залогинены! Введите логин и пароль или зарегистрируйтесь!</font></h1></div>");

}
%>
<jsp:include page="HF/Footer.jsp"></jsp:include>