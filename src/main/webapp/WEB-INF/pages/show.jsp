<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="HF/Header.jsp"></jsp:include>
    <title>Объявление</title>
<div class="container">
    <h3>${advs.name}</h3>
    <c:if test="${advs.photo ne null}">
    <img height="480" width="640" src="${pageContext.servletContext.contextPath}/image/${advs.photo.id}" />
    </c:if>
        <div class="form-group">Категория: <b>${advs.category.name}</b></div>
        <div class="form-group">Наименование: ${advs.name}</div>
        <div class="form-group">Краткое описание: ${advs.shortDesc}</div>
        <div class="form-group">Описание: ${advs.longDesc}</div>
        <div class="form-group">Телефон: ${advs.phone}</div>
        <div class="form-group">Цена: ${advs.price}</div>
</div>
<jsp:include page="HF/Footer.jsp"></jsp:include>
