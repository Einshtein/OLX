<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="HF/Header.jsp"></jsp:include>
<html>
<head>
    <title>Ваш профиль</title>
</head>
<body>
<div class="container">
        <h3>Редактирование объявления:</h3>

        <form class="form-horizontal"  enctype="multipart/form-data" role="form" action="${pageContext.servletContext.contextPath}/MyProfile/updateAdv"
              method="post" accept-charset="utf-8">
                <div class="form-group">Категория: <b>${advs.category.name}</b> (Нельзя изменить!)</div>
                <input type="hidden" value="${advs.id}" name="id"/>
                <div class="form-group">Наименование:</div>
                <div class="form-group"><input type="text" class="form-control" name="name" placeholder="Название" value="${advs.name}"
                                               required style="width: 90%; display: inline"></div>
                <div class="form-group">Краткое описание:</div>
                <div class="form-group"><input type="text" class="form-control" name="shortDesc" placeholder="Краткое описание" value="${advs.shortDesc}"
                                               required style="width: 90%; display: inline"></div>
                <div class="form-group">Описание:</div>
                <div class="form-group"><textarea class="form-control" name="longDesc" placeholder="Описание"  style="width: 90%; display: inline">${advs.longDesc}</textarea>
                </div>
                <div class="form-group">Телефон:</div>
                <div class="form-group"><input type="tel" pattern="[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}" class="form-control"  value="${advs.phone}"
                                               name="phone" placeholder="Телефон в формате (ХХХ)ХХХ-ХХ-ХХ" required style="width: 300px; display: inline"></div>
                <div class="form-group">Цена:</div>
                <div class="form-group"><input type="number" min="0" max="100000" step="0.1"  value="${advs.price}" class="form-control"
                                               name="price" placeholder="Цена°" required style="width: auto; display: inline"></div>
                <div class="form-group">
                        <c:if test="${advs.photo ne null}">
                        Фото: <img height="480" width="640" src="${pageContext.servletContext.contextPath}/image/${advs.photo.id}" />
                        </c:if>
                        <div> Изменить Фото:</div> <input type="file" name="photo" style="display:inline"></div>
                <div class="form-group"><input type="submit" class="btn btn-primary " value="Редактировать"></div>
        </form>
</div>
<jsp:include page="HF/Footer.jsp"></jsp:include>