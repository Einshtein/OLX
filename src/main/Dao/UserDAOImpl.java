package main.Dao;

import main.Model.Basket;
import main.Model.User;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class UserDAOImpl
  implements UserDAO
{
  @Autowired
  private EntityManager entityManager;
  
  public void add(User user)
  {
    try
    {
      this.entityManager.getTransaction().begin();
      this.entityManager.persist(user);
      this.entityManager.getTransaction().commit();
    }
    catch (Exception ex)
    {
      this.entityManager.getTransaction().rollback();
      ex.printStackTrace();
    }
  }
  
  public void delete(Integer id)
  {
    try
    {
      this.entityManager.getTransaction().begin();
      User user = (User)entityManager.find(User.class, Integer.valueOf(id));
      this.entityManager.remove(user);
      this.entityManager.getTransaction().commit();
    }
    catch (Exception ex)
    {
      this.entityManager.getTransaction().rollback();
      ex.printStackTrace();
    }
  }
  
  public User getUserById(Integer id)
  {
    Query query = this.entityManager.createQuery("SELECT a FROM User a WHERE a.id = :id", Basket.class);
    query.setParameter("id", id);
    return (User)query.getResultList().get(0);
  }
  
  public List<User> getAllUsers()
  {
    Query query = entityManager.createQuery("SELECT a FROM User a", User.class);
    return query.getResultList();
  }
  
  public User getUserByLogin(String login)
  {
    Query query = entityManager.createQuery("SELECT a FROM User a WHERE a.login = :login", User.class);
    query.setParameter("login", login);
    return (User)query.getResultList().get(0);
  }
}
