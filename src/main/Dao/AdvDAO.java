package main.Dao;

import main.Model.Advertisement;
import main.Model.AdvertisementAll;
import main.Model.Basket;
import main.Model.Category;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface AdvDAO
{
    //Получаем все объявления на главной странице
   List<AdvertisementAll> getAdv();
    //Получаем все объявления на главной странице по id
   AdvertisementAll getAdvById(long id);
    //Получаем все объявления на главной странице по названию или описанию
   List<AdvertisementAll> getAdv(String pattern);
    //Получаем все объявления пользователя по id
   Advertisement getUserAdvById(long id);
    //Получаем все объявления пользователя по id пользователя
   List<Advertisement> getUserAdvByUserId(long id);
    //Получаем все объявления пользователя по названию или описанию
   List<Advertisement> getUserAdvByPattern(String pattern);
    //Получаем все удаленные объявления пользователя по id пользователя
   List<Basket> getAdvFromBasketByUserId(long id);
    //Получаем все удаленные объявления пользователя по названию или описанию и id пользователя
   List<Basket> getAllBasketAdv(String pattern, long id);
    //Получаем все категории объявлений
    List<Category> getAllCategory ();
    //Получаем категории объявлений по названию
   Category getCategoryByName(String name);
    //Добавление объявления
   void addAdv(Advertisement paramAdvertisement);
    //Удаление объявления
   void deleteAdv(long paramLong, HttpServletRequest request);
    //Удаление нескольких объявления по чекбоксам
   void deleteAdvByCheckbox(List<Long> idList, HttpServletRequest request);
    //Восстановление объявления
   void restoreAdv(long paramLong, HttpServletRequest request);
    //Восстановление нескольких объявления по чекбоксам
   void restoreAdvByCheckbox(List<Long> idList, HttpServletRequest request);
    //Подгрузка фото
  byte[] getPhoto(long id);
    //Окончательное удаление объявления
  void truncateAdv(long id);
}
