package main.Dao;

import main.Model.User;

import java.util.List;

public abstract interface UserDAO
{
  //Добавление пользователя
  void add(User user);
  //Удаление пользователя
  void delete(Integer id);
  //получение пользователя по id
  User getUserById(Integer id);
  //Получение всех пользователей
  List<User> getAllUsers();
  //Получение пользователя по логину
  User getUserByLogin(String login);
}
