package main.Dao;

import main.Model.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class AdvDAOImpl implements AdvDAO {
  @Autowired
  private EntityManager entityManager;

  public static boolean success;
  public List<AdvertisementAll> getAdv()
  {
    Query query = entityManager.createQuery("SELECT a FROM AdvertisementAll a ORDER BY name", AdvertisementAll.class);
    return query.getResultList();
  }

  @Override
  public AdvertisementAll getAdvById(long id) {
    Query query = entityManager.createQuery("SELECT a FROM AdvertisementAll a WHERE a.id = :id ORDER BY name", AdvertisementAll.class);
    query.setParameter("id", Long.valueOf(id));
    return (AdvertisementAll) query.getResultList().get(0);
  }
  @Override
  public List<AdvertisementAll> getAdv(String pattern)
  {
    Query query = entityManager.createQuery("SELECT a FROM AdvertisementAll a WHERE a.shortDesc LIKE :pattern ORDER BY name", AdvertisementAll.class);
    query.setParameter("pattern", "%" + pattern + "%");
    return query.getResultList();
  }
  @Override
  public Advertisement getUserAdvById(long id)
  {
    Query query = entityManager.createQuery("SELECT a FROM Advertisement a WHERE a.id = :id ORDER BY name", Advertisement.class);
    query.setParameter("id", Long.valueOf(id));
    return (Advertisement) query.getResultList().get(0);
  }
  @Override
  public List<Advertisement> getUserAdvByUserId(long id)
  {
    Query query = entityManager.createQuery("SELECT a FROM Advertisement a WHERE a.user.id = :id ORDER BY name", Advertisement.class);
    query.setParameter("id", Long.valueOf(id));
    return query.getResultList();
  }
  @Override
  public List<Advertisement> getUserAdvByPattern(String pattern)
  {
    Query query = entityManager.createQuery("SELECT a FROM Advertisement a WHERE a.shortDesc LIKE :pattern ORDER BY name", Advertisement.class);
    query.setParameter("pattern", "%" + pattern + "%");
    return query.getResultList();
  }
  @Override
  public List<Basket> getAllBasketAdv(String pattern, long id)
  {
    Query query = entityManager.createQuery("SELECT a FROM Basket a WHERE a.user.id = :id and a.shortDesc LIKE :pattern or a.name LIKE :pattern ORDER BY name", Basket.class);
    query.setParameter("id", Long.valueOf(id));
    query.setParameter("pattern", "%" + pattern + "%");
    return query.getResultList();
  }

  @Override
  public List<Category> getAllCategory() {
    Query query = entityManager.createQuery("SELECT a FROM Category a ORDER BY name", Category.class);
    return query.getResultList();
  }

  @Override
  public Category getCategoryByName(String name) {
    Query query = entityManager.createQuery("SELECT a FROM Category a WHERE a.name = :name ORDER BY name", Category.class);
    query.setParameter("name", name);
    return (Category)query.getResultList().get(0);
  }
  @Override
  public List<Basket> getAdvFromBasketByUserId(long id)
  {
    Query query = entityManager.createQuery("SELECT a FROM Basket a WHERE a.user.id = :id ORDER BY name", Basket.class);
    query.setParameter("id", Long.valueOf(id));
    return query.getResultList();
  }
  @Override
  public void addAdv(Advertisement adv)
  {
    try
    {
      entityManager.getTransaction().begin();
      entityManager.persist(adv);
      entityManager.getTransaction().commit();
      success = true;
    }
    catch (Exception ex)
    {
      entityManager.getTransaction().rollback();
      ex.printStackTrace();
      success = false;
    }

  }
  @Override
  public void deleteAdv(long id, HttpServletRequest request)
  {
    try
    {
      entityManager.getTransaction().begin();
      Advertisement adv = (Advertisement)entityManager.find(Advertisement.class, Long.valueOf(id));
      Basket basket = new Basket(adv.getName(), adv.getShortDesc(), adv.getLongDesc(), adv.getPhone(), adv.getPrice(), adv.getPhoto());
      User user = (User)request.getSession().getAttribute("user");
      Category category = adv.getCategory();
      basket.setUser(user);
      basket.setCategory(category);
      entityManager.remove(adv);
      entityManager.persist(basket);
      entityManager.getTransaction().commit();
      success = true;
    }
    catch (Exception ex)
    {
      entityManager.getTransaction().rollback();
      ex.printStackTrace();
      success = false;
    }
  }
  @Override
  public void deleteAdvByCheckbox(List<Long> idList, HttpServletRequest request)
  {
    try
    {
      entityManager.getTransaction().begin();
      for (Long id : idList)
      {
        Advertisement adv = (Advertisement)entityManager.find(Advertisement.class, id);
        Basket basket = new Basket(adv.getName(), adv.getShortDesc(), adv.getLongDesc(), adv.getPhone(), adv.getPrice(), adv.getPhoto());
        User user = (User)request.getSession().getAttribute("user");
        Category category = adv.getCategory();
        basket.setUser(user);
        basket.setCategory(category);
        entityManager.remove(adv);
        entityManager.persist(basket);
        success = true;
      }
      entityManager.getTransaction().commit();
    }
    catch (Exception ex)
    {
      entityManager.getTransaction().rollback();
      ex.printStackTrace();
      success = false;
    }
  }
  @Override
  public void restoreAdv(long id, HttpServletRequest request)
  {
    try
    {
      entityManager.getTransaction().begin();
      Basket basket = (Basket)entityManager.find(Basket.class, Long.valueOf(id));
      Advertisement adv = new Advertisement(basket.getName(), basket.getShortDesc(), basket.getLongDesc(), basket.getPhone(), basket.getPrice(),
              basket.getPhoto());
      User user = (User)request.getSession().getAttribute("user");
      Category category = basket.getCategory();
      adv.setUser(user);
      adv.setCategory(category);
      entityManager.remove(basket);
      entityManager.persist(adv);
      entityManager.getTransaction().commit();
      success = true;
    }
    catch (Exception ex)
    {
      entityManager.getTransaction().rollback();
      ex.printStackTrace();
      success = false;
    }
  }
  @Override
  public void restoreAdvByCheckbox(List<Long> idList, HttpServletRequest request)
  {
    try
    {
      entityManager.getTransaction().begin();
      for (Long id : idList)
      {
        Basket basket = (Basket)entityManager.find(Basket.class, id);
        Advertisement adv = new Advertisement(basket.getName(), basket.getShortDesc(), basket.getLongDesc(), basket.getPhone(),
                basket.getPrice(), basket.getPhoto());
        User user = (User)request.getSession().getAttribute("user");
        Category category = basket.getCategory();
        adv.setUser(user);
        adv.setCategory(category);
        entityManager.remove(basket);
        entityManager.persist(adv);
        success = true;
      }
      entityManager.getTransaction().commit();
    }
    catch (Exception ex)
    {
      this.entityManager.getTransaction().rollback();
      ex.printStackTrace();
      success = false;
    }
  }
  @Override
  public byte[] getPhoto(long id)
  {
    try
    {
      Photo photo = (Photo)entityManager.find(Photo.class, Long.valueOf(id));
      return photo.getBody();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    return null;
  }
  @Override
  public void truncateAdv(long id)
  {
    try
    {
      this.entityManager.getTransaction().begin();
      Basket basket = (Basket)entityManager.find(Basket.class, Long.valueOf(id));
      this.entityManager.remove(basket);
      this.entityManager.getTransaction().commit();
      success = true;
    }
    catch (Exception ex)
    {
      this.entityManager.getTransaction().rollback();
      ex.printStackTrace();
      success = false;
    }
  }
}
